<?php
ini_set('display_errors', 1);
require_once('env.php');
require_once('TwitterAPIExchange.php');

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
    'oauth_access_token' => oauth_access_token,
    'oauth_access_token_secret' => oauth_access_token_secret,
    'consumer_key' => consumer_key,
    'consumer_secret' => consumer_secret
);

//calc angle
$today = new DateTime();

$totalcrenaux = 7*24;
$nowstate = intval(date_format($today, 'w'))*24 + intval(date_format($today, 'G'));
$degres = ($nowstate / $totalcrenaux)*360;

//gestion image
$source = imagecreatefromjpeg(__DIR__.'/profil.jpg');
$rotate = imagerotate($source, $degres, 0);
$crop = imagecrop($rotate, [
    'x' => (imagesx($rotate)/2)-200,
    'y' => (imagesx($rotate)/2)-200,
    'width' => 400,
    'height' => 400,
]);

imagejpeg($crop, __DIR__.'/yolo.jpg');
$type = pathinfo(__DIR__.'/yolo.jpg', PATHINFO_EXTENSION);
$data = file_get_contents(__DIR__.'/yolo.jpg');
$base64 = base64_encode($data);

imagedestroy($source);
imagedestroy($rotate);
imagedestroy($crop);


/** URL for REST request, see: https://dev.twitter.com/docs/api/1.1/ **/
$url = 'https://api.twitter.com/1.1/account/update_profile_image.json';
$requestMethod = 'POST';

/** POST fields required by the URL above. See relevant docs as above **/
$postfields = array(
    'name' => 'yolo'.date_format($today, 'Y-m-d_H'),
    'image' => $base64
);

/** Perform a POST request and echo the response **/
$twitter = new TwitterAPIExchange($settings);
$twitter->buildOauth($url, $requestMethod)
    ->setPostfields($postfields)
    ->performRequest();