<?php

//calc angle
$today = new DateTime();

$totalcrenaux = intval(date_format($today, 't'))*24;
$nowstate = intval(date_format($today, 'j'))*24 + intval(date_format($today, 'G'));
$degres = ($nowstate / $totalcrenaux)*360;

//gestion image
$source = imagecreatefromjpeg('profil.jpg');
$rotate = imagerotate($source, $degres, 0);
$crop = imagecrop($rotate, [
    'x' => (imagesx($rotate)/2)-200,
    'y' => (imagesx($rotate)/2)-200,
    'width' => 400,
    'height' => 400,
]);

imagejpeg($crop, 'yolo.jpg');
$type = pathinfo('yolo.jpg', PATHINFO_EXTENSION);
$data = file_get_contents('yolo.jpg');
$base64 = 'data:image/'.$type.';base64,'.base64_encode($data);

print($base64);

imagedestroy($source);
imagedestroy($rotate);
imagedestroy($crop);